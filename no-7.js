const dataPenjualanPakAldi = [
    {
        namaProduct: 'Sepatu Futsal Nike Vapor Academy 8',
        hargaSatuan: 760000,
        kategori: "Sepatu Sport",
        totalTerjual: 90,
    },
    {
        namaProduct: 'Sepatu Warrior Tristan Black Brown High',
        hargaSatuan: 960000,
        kategori: "Sepatu Sneaker",
        totalTerjual: 37,
    },
    {
        namaProduct: 'Sepatu Warrior Tristan Maroon High ',
        kategori: "Sepatu Sneaker",
        hargaSatuan: 360000,
        totalTerjual: 90,
    },
    {
        namaProduct: 'Sepatu Warrior Rainbow Tosca Corduroy',
        hargaSatuan: 120000,
        kategori: "Sepatu Sneaker",
        totalTerjual: 90,
    }
]

const getTotalPenjualan = (dataPenjualan) => {
    // Validasi tipe data input
    if (dataPenjualan == null) return "Error: Bro where is the parameter?";
    if (typeof dataPenjualan !== "object") return "Error: Parameter bukan array";

    const sum = dataPenjualan.map(item => item.totalTerjual).reduce((prev, curr) => prev + curr, 0);
    return sum
}

console.log(getTotalPenjualan(dataPenjualanPakAldi));
console.log(getTotalPenjualan(0));
console.log(getTotalPenjualan());