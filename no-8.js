const dataPenjualanNovel = [
    {
        idProduct: 'BOOK002421',
        namaProduk: 'Pulang - Pergi',
        penulis: 'Tere Liye',
        hargaBeli: 60000,
        hargaJual: 86000,
        totalTerjual: 150,
        sisaStok: 17,
    },
    {
        idProduct: 'BOOK002351',
        namaProduk: 'Selamat Tinggal',
        penulis: 'Tere Liye',
        hargaBeli: 75000,
        hargaJual: 103000,
        totalTerjual: 171,
        sisaStok: 20,
    },
    {
        idProduct: 'BOOK002941',
        namaProduk: 'Garis Waktu',
        penulis: 'Fiersa Besari',
        hargaBeli: 67000,
        hargaJual: 99000,
        totalTerjual: 213,
        sisaStok: 5,
    },
    {
        idProduct: 'BOOK002941',
        namaProduk: 'Laskar Pelangi',
        penulis: 'Andrea Hirata',
        hargaBeli: 55000,
        hargaJual: 68000,
        totalTerjual: 20,
        sisaStok: 56,
    },
];

const getInfoPenjualan = (dataPenjualan) => {
    // Validasi Input
    if (dataPenjualan == null) return "Error: Bro where is the parameter?";
    if (typeof dataPenjualan !== "object") return "Error: Parameter bukan array";

    // Reserved object untuk output
    const myObj = {
        totalKeuntungan: null,
        totalModal: null,
        persentaseKeuntungan: null,
        produkBukuTerlaris: null,
        penulisTerlaris: null
    }

    let totalPenjualan = 0;
    let totalKeuntungan = 0;
    let totalModal = 0;
    let persentaseKeuntungan = 0;

    // Menghitung total penjualan dan modal untuk mencari keuntungan & persentase
    for (let i = 0; i < dataPenjualan.length; i++) {
        totalPenjualan += (dataPenjualan[i].hargaJual * dataPenjualan[i].totalTerjual)
        totalModal += (dataPenjualan[i].hargaBeli * dataPenjualan[i].totalTerjual)
    }

    totalKeuntungan = totalPenjualan - totalModal;
    persentaseKeuntungan = totalPenjualan / totalModal * 100;

    // Mencari buku terlaris
    const jumlahTerjual = dataPenjualan.map(obj => obj.totalTerjual)
    const maxTerjual = Math.max(...jumlahTerjual);
    const bukuTerlaris = dataPenjualan.find(obj => obj.totalTerjual === maxTerjual);
    const produkTerlaris = bukuTerlaris.namaProduk;

    // Mengurutkan penulis terlaris
    const penulisTerlaris = dataPenjualan.reduce((prev, curr) => {
        const index = prev.findIndex(search => search.penulis === curr.penulis)

        if (index !== -1) {
            const data = [...prev]
            data[index].totalTerjual += curr.totalTerjual
            return [...data]
        }

        return [...prev, curr]

    }, [])

    // Mencari penulis terlaris
    const penulisTerjual = penulisTerlaris.map(obj => obj.totalTerjual)
    const maxPenulisTerjual = Math.max(...penulisTerjual);
    const penulisTerlarisFix = dataPenjualan.find(obj => obj.totalTerjual === maxPenulisTerjual);
    const namaPenulisTerlaris = penulisTerlarisFix.penulis;

    myObj.totalKeuntungan = new Intl.NumberFormat("id-ID", {style: "currency", currency: "IDR"}).format(totalKeuntungan);
    myObj.totalModal = new Intl.NumberFormat("id-ID", {style: "currency", currency: "IDR"}).format(totalModal);
    myObj.persentaseKeuntungan = persentaseKeuntungan.toFixed(2) + "%";
    myObj.produkBukuTerlaris = produkTerlaris;
    myObj.penulisTerlaris = namaPenulisTerlaris;


    return myObj;
}

console.log(getInfoPenjualan(dataPenjualanNovel))