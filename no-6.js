const getAngkaTerbesarKedua = (dataNumbers) => {
    // Validasi tipe data pertama
    if (dataNumbers == null) return "Error: Bro where is the parameter?";
    if (typeof dataNumbers !== "object") return "Error: Parameter bukan array";
    
    // Mengeliminasi angka yang sama dengan menggunakan set
    const setAngka = new Set(dataNumbers);
    // Convert set ke array
    const uniqueArr = Array.from(setAngka);

    // Sorting descending dan return nilai terbesar kedua
    uniqueArr.sort(function(a, b){return b - a});
    return uniqueArr[1];
}

const dataAngka = [9,4,7,7,4,3,2,2,8];

console.log(getAngkaTerbesarKedua(dataAngka));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());