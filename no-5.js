const getSplitName = (personName) => {
    const myObj = {
        firstName: null,
        middleName: null,
        lastName: null
    }

    if (typeof personName == "number") return "Error: Invalid data type"
    if (typeof personName == "string") {
        const arr = personName.split(" ");
        if (arr.length > 3) return "Error: This function is only for 3 characters name"
        if (arr.length == 3) {
            myObj.firstName = arr[0]
            myObj.middleName = arr[1]
            myObj.lastName = arr[2]
            return myObj
        }
        if (arr.length == 2) {
            myObj.firstName = arr[0]
            myObj.middleName = null
            myObj.lastName = arr[1]
            return myObj
        }
        if (arr.length == 1) {
            myObj.firstName = arr[0]
            myObj.middleName = null
            myObj.lastName = null
            return myObj
        }
        
    }

}

console.log(getSplitName("Alvian Tedy Aditya"));
console.log(getSplitName("Alvian Tedy Aditya Sinaga"));
console.log(getSplitName("Alvian"));
console.log(getSplitName("Alvian Tedy"));
console.log(getSplitName(3));