const isValidPassword = (givenPassword) => {
    // Regex kondisi sesuai soal
    const regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/; // 1 Upper 1 Lower 1 Number
    
    // Validasi input
    if (typeof givenPassword == "number") return "Error: Password tidak boleh berisi angka saja";
    if (givenPassword == null) return "Error: Bro where is the parameter?";
    if (regex.test(givenPassword)) return true;
    else return false;


}

console.log(isValidPassword("Meong2021"));
console.log(isValidPassword("Meg2021"));
console.log(isValidPassword("meong2021"));
console.log(isValidPassword("@eong"));
console.log(isValidPassword("Meong2"));
console.log(isValidPassword(0));
console.log(isValidPassword());