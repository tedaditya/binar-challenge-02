const checkTypeNumber = (givenNumber) => {
    // Validasi input
    if (givenNumber == null) return "Error: Bro where is the parameter?";
    if (typeof givenNumber !== "number") return "Error: Invalid Data Type";
    if (typeof givenNumber == "number") {
        if (givenNumber % 2 == 0) {
            return "GENAP";
        } else {
            return "GANJIL";
        }
    }
    else return "Error: Salah input ya bro?"

}


console.log(checkTypeNumber(1))
console.log(checkTypeNumber(10))
console.log(checkTypeNumber({}))
console.log(checkTypeNumber([]))
console.log(checkTypeNumber())