const checkEmail = (email) => {
    const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    
    // Validasi input
    if (email == null) return "Error: Bro where is the parameter?";
    if (typeof email == "number") return "ERROR: Tipe data yang dimasukkan salah"
    if (!email.includes("@") || !email.includes("."))return "ERROR: Format email yang benar harus ada @ dan .";
    if (regex.test(email)) return "VALID";
    else return "INVALID";
}

console.log(checkEmail("tedaditya@gmail.co.id"));
console.log(checkEmail("tedaditya@gmail.com"));
console.log(checkEmail("tedaditya@asik"));
console.log(checkEmail("tedaditya"));
console.log(checkEmail());
console.log(checkEmail(333));